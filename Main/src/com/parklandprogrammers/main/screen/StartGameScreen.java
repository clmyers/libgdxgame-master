package com.parklandprogrammers.main.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.TimeUtils;
import com.parklandprogrammers.main.InputHandler;
import com.parklandprogrammers.main.MyGame;
import com.parklandprogrammers.main.actor.BucketActor;
import com.parklandprogrammers.main.actor.DropActor;

/**
 * Created by aguggenberger on 3/4/14.
 */
public class StartGameScreen extends AbstractScreen {
    private BucketActor bucket;
    private DropActor drop;
    private InputHandler myinput;
    private long lastRainDropTime;
    private BitmapFont font;
    private Label text;
    private Label.LabelStyle textStyle;
    private Music rainMusic;
    private Sound dropSound;
    private int score;

    public StartGameScreen(MyGame game) {
        super(game);
        score = 0;
        font = new BitmapFont();
        textStyle = new Label.LabelStyle();
        textStyle.font = font;
        text = new Label("Score: " + score, textStyle);
        text.setBounds(20, 460, 2, 2);
        text.setFontScale(1f, 1f);

        rainMusic = Gdx.audio.newMusic(Gdx.files.internal("sfx/rain.mp3"));
        rainMusic.play();
        rainMusic.setLooping(true);

        dropSound = Gdx.audio.newSound(Gdx.files.internal("sfx/drop.wav"));

        drop = new DropActor();
        bucket = new BucketActor();
        bucket.setTouchable(Touchable.enabled);
        myinput = new InputHandler(bucket);
        stage.addActor(bucket);
        stage.addActor(text);
        stage.addListener(myinput);
        spawnRaindrop();
        //stage.addActor(drop);
    }

    private void spawnRaindrop(){
        DropActor dropActor = new DropActor();
        dropActor.setPosition(MathUtils.random(0, 800 - 64), 480);
        dropActor.addAction(Actions.moveTo(dropActor.getX(), -50, 10));
        dropActor.setTouchable(Touchable.enabled);
        stage.addActor(dropActor);
        lastRainDropTime = TimeUtils.nanoTime();
    }

    @Override
    public void render(float delta) {
        update(delta);
        super.render(delta);
    }

    private void update(float delta){
        for (Actor actor: stage.getActors()){
            if (actor != bucket){
                if (checkCollision(actor, bucket)){
                    actor.remove();
                    score++;
                    dropSound.play();
                }
                else if (actor.getY() <= 0){
                    actor.remove();
                    score--;
                }
                if (score >= 0)
                    text.setText("Score: " + score);
                else
                    score = 0;
            }
        }

        if (((TimeUtils.nanoTime() - lastRainDropTime) / 1000000000) > 3)
            spawnRaindrop();
    }

    private boolean checkCollision(Actor myDrop, BucketActor myBucket){
        Rectangle dropRectangle = new Rectangle(myDrop.getX(), myDrop.getY(),
                myDrop.getWidth(), myDrop.getHeight());
        Rectangle bucketRectangle = new Rectangle(myBucket.getX(), myBucket.getY(),
                myBucket.getWidth(), myBucket.getHeight());
        return Intersector.overlaps(dropRectangle, bucketRectangle);
    }
}
