package com.parklandprogrammers.main;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.parklandprogrammers.main.actor.BucketActor;

/**
 * Created by worklaptop on 3/6/14.
 */
public class InputHandler extends InputListener{
    BucketActor bucket;

    public InputHandler(BucketActor bucket){
        this.bucket = bucket;
    }

    @Override
    public boolean keyDown(InputEvent event, int keycode) {
        switch(keycode){
            case Input.Keys.DPAD_RIGHT:
                bucket.moveRight(10f);
                break;
            case Input.Keys.DPAD_LEFT:
                bucket.moveLeft(10f);
                break;
            default:
                break;
        }
        return true;
    }
}
